import { render } from '@testing-library/react';
import App from './App';

test('renders two inputs', () => {
  const {container} = render(<App />);
  expect(container.querySelectorAll('form').length).toBe(2)
});
