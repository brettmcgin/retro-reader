import PropTypes from 'prop-types';
import './reader.css'

function Reader({content, simpleHighlights}) {
  const span = (string, color) => `<span style="color: ${color}">${string}</span>`

  const updated = () => content.replace(simpleHighlights.subString, span(simpleHighlights.subString, simpleHighlights.color))

  console.log({content: updated()})

  return (
    <div dangerouslySetInnerHTML={{__html: updated()}} className="reader"></div>
  );
}

Reader.propTypes = {
  content: PropTypes.string,
  simpleHighlights: PropTypes.shape({
    subString: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,
}

export default Reader;
