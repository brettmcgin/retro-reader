import PropTypes from 'prop-types';
import './App.css';
import { useState } from 'react';

function Input({initial, onInputChanged}) {
  const [input, setInput] = useState("")

  function handleSubmit(event) {
    event.preventDefault();
  }

  function handleOnChange(value) {
    setInput(value)
    onInputChanged(value)
  }

  return (
    <form onSubmit={handleSubmit}>
      <label><textarea value={input} onChange={e => handleOnChange(e.target.value)} /></label>
      <input type="submit" value="Submit" />
    </form>
  );
}

Input.propTypes = {
  initial: PropTypes.string,
  onInputChanged: PropTypes.func.isRequired,
}

Input.defaultProps = {
  initial: ""
}

export default Input;
