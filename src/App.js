import './App.css';
import { useState } from 'react';
import Input from './Input';
import Reader from './Reader';

function App() {
  const [input, setInput] = useState("")
  const [config, setConfig] = useState("")

  console.log(config)

  return (
    <div className="App">
      <Input onInputChanged={(stuff) => setInput(stuff)} />
      <Input onInputChanged={(stuff) => setConfig(stuff)} />
      <Reader content={input} simpleHighlights={{
        subString: "Red XIII",
        color: "Red"
      }} />
    </div>
  );
}

export default App;
